# README

# Oyster Card

## Installation
```
bundle install
```

## Run Server
```
rails s
```

## Run tests
```
rails test
```

## Execute the task Journey and Fare
```
curl http://localhost:3000
```

## Run Test of the task Journey
```
rails test test/models/travel_test.rb -n test_Should_start_travel_oyster_card_challenge
```


## Console
### After execution the console will be return:
```
----------------------------------------------
------------- Oyster Card System -------------
----------------------------------------------
-----> Station - Touched In
-----> Card Pre Charge:  £3.20
-----> Card Pre Balance: £26.80
----->
----->     Journey :
----->         From: Holborn - Zone: 1
----->         To:   Earl’s Court - Zone: 1
----->         By:   tube
----->         Cost: £2.50
----->
-----> Station - Touched Out
-----> Card Refund:  £3.20
-----> Card Balance: £27.50
----->
----->
-----> Station - Touched In
-----> Card Pre Charge:  £3.20
-----> Card Pre Balance: £24.30
----->
----->     Journey :
----->         From: Earl’s Court
----->         To:   Chelsea
----->         By:   bus
----->         Cost: £2.00
----->
-----> Station - Touched Out
-----> Card Refund:  £3.20
-----> Card Balance: £25.50
----->
----->
-----> Station - Touched In
-----> Card Pre Charge:  £3.20
-----> Card Pre Balance: £22.30
----->
----->     Journey :
----->         From: Earl’s Court - Zone: 2
----->         To:   Hammersmith - Zone: 2
----->         By:   tube
----->         Cost: £2.00
----->
-----> Station - Touched Out
-----> Card Refund:  £3.20
-----> Card Balance: £23.50
----->
----->
----------------------------------------------
------------- Oyster Card System -------------
----------------------------------------------
```

## Credits: Raul Klumpp <raulklumpp@gmail.com>
