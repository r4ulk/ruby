require 'test_helper'

class TravelTest < ActiveSupport::TestCase

  setup do
    @station = Station.new({})
    @journeys = [
      Journey.new([@station.holborn, @station.earls_court_zone_one], Transport.tube),
      Journey.new([@station.earls_court_zone_one, @station.chelsea], Transport.bus),
      Journey.new([@station.earls_court_zone_two, @station.hammersmith], Transport.tube),
    ]
    @card = Card.new(30)
  end

  test "Should start travel with empty journey" do
    travel = Travel.new([], @card)
    assert_not travel.start()
  end

  test "Should start travel oyster card challenge" do
    @travel = Travel.new(@journeys, @card)
    @travel.start()
    assert_equal 23.7, @card.balance
  end

end
