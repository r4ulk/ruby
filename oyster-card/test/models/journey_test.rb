require 'test_helper'

class JourneyTest < ActiveSupport::TestCase

  setup do
    @station = Station.new({})
  end

  test "Should equal zones" do
    journey = Journey.new([@station.holborn, @station.holborn], Transport.tube)
    assert journey.has_equals_zone
  end

  test "Should not equal zones" do
    journey = Journey.new([@station.holborn, @station.earls_court_zone_two], Transport.tube)
    assert_not journey.has_equals_zone
  end

  test "Should has zone number one" do
    journey = Journey.new([@station.holborn, @station.earls_court_zone_two], Transport.tube)
    assert journey.has_zone_one
  end

  test "Should hasnt zone number one" do
    journey = Journey.new([@station.hammersmith, @station.wimbledon], Transport.tube)
    assert_not journey.has_zone_one
  end

  test "Should count one zone" do
    journey = Journey.new([@station.holborn, @station.holborn], Transport.tube)
    assert_equal 1, journey.zones_count
  end

  test "Should count two zones" do
    journey = Journey.new([@station.holborn, @station.earls_court_zone_two], Transport.tube)
    assert_equal 2, journey.zones_count
  end

  test "Should count three zones" do
    journey = Journey.new([@station.holborn, @station.earls_court_zone_two, @station.wimbledon], Transport.tube)
    assert_equal 3, journey.zones_count
  end

end
