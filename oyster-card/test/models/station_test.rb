require 'test_helper'

class StationTest < ActiveSupport::TestCase

  setup do
    @station = Station.new({})
  end

  test "Should get holborn station" do
    station = @station.holborn
    assert_equal "Holborn", station.name
    assert_equal 1, station.zone
  end

  test "Should get earls court station zone one" do
    station = @station.earls_court_zone_one
    assert_equal "Earl's Court", station.name
    assert_equal 1, station.zone
  end

  test "Should get wimbledon station" do
    station = @station.wimbledon
    assert_equal "Wimbledon", station.name
    assert_equal 3, station.zone
  end

  test "Should get hammersmith station" do
    station = @station.hammersmith
    assert_equal "Hammersmith", station.name
    assert_equal 2, station.zone
  end

end
