require 'test_helper'

class CardTest < ActiveSupport::TestCase

  setup do
    @card = Card.new(30)
    @fare = Fare.new()
  end

  test "Should has £30 on balance" do
    assert_equal 30, @card.balance
  end

  test "Should to charge maximum fare" do
    @card.to_charge(@fare.max)
    assert_equal 26.8, @card.balance
  end

  test "Should to recharge maximum fare" do
    card = Card.new(26.8)
    card.refund(@fare.max)
    assert_equal 30, card.balance
  end

  test "Should has enough" do
    card = Card.new(10)
    assert card.has_enough(@fare.max)
  end

  test "Should hasn't enough" do
    card = Card.new(3)
    assert_not card.has_enough(@fare.max)
  end

end
