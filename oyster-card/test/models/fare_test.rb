require 'test_helper'

class FareTest < ActiveSupport::TestCase

  setup do
    @card = Card.new(30)
    @fare = Fare.new()
    @station = Station.new({})
  end

  test "Should give fare anywhere in zone 1" do
    journey = Journey.new([@station.holborn(), @station.earls_court_zone_one()], Transport.tube)
    assert_equal 2.5, @fare.price(journey)
  end

  test "Should give fare any one zone outside zone 1" do
    journey = Journey.new([@station.hammersmith(), @station.earls_court_zone_two()], Transport.tube)
    assert_equal 2, @fare.price(journey)
  end

  test "Should give fare any two zones including zone 1" do
    journey = Journey.new([@station.holborn(), @station.earls_court_zone_two()], Transport.tube)
    assert_equal 3, @fare.price(journey)
  end

  test "Should give fare any two zones excluding zone 1" do
    journey = Journey.new([@station.hammersmith(), @station.wimbledon()], Transport.tube)
    assert_equal 2.25, @fare.price(journey)
  end

  test "Should give bus fare" do
    journey = Journey.new([@station.earls_court_zone_one(), @station.earls_court_zone_one()], Transport.bus)
    assert_equal 1.8, @fare.price(journey)
  end

end
