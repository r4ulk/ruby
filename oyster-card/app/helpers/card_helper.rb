module CardHelper
  include ActionView::Helpers::NumberHelper

  def print_out_card_balance(balance)
    puts '----->'
    puts '-----> Card Loaded:      ' + number_to_currency(balance, unit: "£")
    puts '-----> '
  end

end
