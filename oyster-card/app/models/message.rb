class Message

  def helper
    @helper ||= Class.new do
      include ActionView::Helpers::NumberHelper
    end.new
  end

  def oyster_card()
    puts "----------------------------------------------"
    puts "------------- Oyster Card System -------------"
    puts "----------------------------------------------"
  end

  def insufficient_funds()
    puts "-----> WARNING: Insufficient Funds"
  end

  def empty_journey()
    puts "-----> Station - Touched In"
    puts "----->"
    puts "-----> No Journey defined"
    puts "----->"
    puts "-----> Station - Touched Out"
  end

  def touched_in(stations, transport, balance, max_fare, fare_price)
    puts "-----> Station - Touched In"
    puts "-----> Card Pre Charge:  " + helper.number_to_currency(max_fare, unit: "£")
    puts "-----> Card Pre Balance: " + helper.number_to_currency(balance, unit: "£")
    puts "-----> "
    puts "----->     Journey : "
    stations.each_with_index { |s, i|
      zone_text  = (!s.zone.nil?) ? " - Zone: " + s.zone.to_s : "".to_s
      if i == 0
        from_to_text = "Start From: "
      elsif i == (stations.count-1)
        from_to_text = "End travel: "
      else
        from_to_text = "Stop " + i.to_s + ":     "
      end
      puts "----->         " + from_to_text + s.name + zone_text.to_s
    }
    puts "----->         Transport:  " + transport
    puts "----->         Cost:       " + helper.number_to_currency(fare_price, unit: "£")
  end

  def touched_out(balance, max_fare)
    puts "-----> "
    puts "-----> Station - Touched Out"
    puts "-----> Card Refund:  " + helper.number_to_currency(max_fare, unit: "£")
    puts "-----> Card Balance: " + helper.number_to_currency(balance, unit: "£")
    puts "-----> "
    puts "-----> "
  end

  def fare_information(balance, fare_price, fare_rules)
    puts "-----> "
    puts "-----> Rules of Fare applied: "
    puts "-----> " + fare_rules.to_s
    puts "-----> "
    puts "-----> Total Fare: " + helper.number_to_currency(fare_price, unit: "£")
    puts "-----> Card Final Balance: " + helper.number_to_currency(balance, unit: "£")
    puts "-----> "
  end

end
