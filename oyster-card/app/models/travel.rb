class Travel

  attr_accessor :journeys
  attr_accessor :card
  attr_accessor :fare

  def initialize(journeys, card)
    @journeys = journeys
    @card = card
    @fare = Fare.new()
    @message = Message.new()
  end

  def start()

    @message.oyster_card

    if !empty_journey
      self.journeys.each { |j|

        fare_price = self.fare.price(j)

        # Insufficient funds
        if !self.card.has_enough(fare_price)
          @message.insufficient_funds
          break
        end

        # Card pre charged with max fare
        self.card.to_charge(self.fare.max)
        @message.touched_in(j.stations,
                            j.transport,
                            self.card.balance,
                            self.fare.max,
                            fare_price)

        # Card refund
        self.card.refund(self.fare.max)

        # Card charged with real fare price
        self.card.to_charge(fare_price)

        @message.touched_out(self.card.balance, self.fare.max)
      }
      @message.fare_information(self.card.balance, self.fare.applied, self.fare.rules)
      @message.oyster_card
    end
    false

  end

  private

  def empty_journey
    if self.journeys.empty?
      @message.empty_journey
      self.card.to_charge(self.fare.max)
      true
    end
    false
  end

end
