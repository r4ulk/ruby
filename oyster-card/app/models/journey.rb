class Journey
  include ActiveModel::Model

  attr_accessor :stations
  attr_accessor :transport

  def initialize(stations, transport)
    @stations, @transport = stations, transport
  end

  def has_equals_zone
    has = false
    zones = []
    self.stations.each { |s|
      if zones.include?(s.zone)
        has = true
        break
      end
      zones.push(s.zone)
    }
    has
  end

  def has_only_zone_one
    has = true
    self.stations.each { |s|
      if !s.zone.eql?(1)
        has = false
        break
      end
    }
    has
  end

  def has_zone_one
    has = false
    self.stations.each { |s|
      if s.zone.eql?(1)
        has = true
        break
      end
    }
    has
  end

  def zones_count
    zones = []
    self.stations.each { |s|
      if zones.include?(s.zone)
        break
      end
      zones.push(s.zone)
    }
    zones.count
  end

end
