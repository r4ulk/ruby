class Station

  attr_accessor :name
  attr_accessor :zone

  def initialize(options = {})
    @name, @zone = options[:name], options[:zone]
  end

  def holborn
    Station.new(name: "Holborn", zone: 1)
  end

  def earls_court_zone_one
    Station.new(name: "Earl's Court", zone: 1)
  end

  def earls_court_zone_two
    Station.new(name: "Earl's Court", zone: 2)
  end

  def wimbledon
    Station.new(name: "Wimbledon", zone: 3)
  end

  def hammersmith
    Station.new(name: "Hammersmith", zone: 2)
  end

  def chelsea
    Station.new(name: "Chelsea", zone: nil)
  end

end
