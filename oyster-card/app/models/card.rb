class Card
  attr_accessor :balance

  def initialize(balance)
    @balance = balance
  end

  def to_charge(fare)
    self.balance -= fare
  end

  def refund(amount)
    self.balance += amount.to_f
  end

  def has_enough(fare)
    (self.balance - fare.to_f) >= 0
  end

end
