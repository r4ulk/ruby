class Fare

  attr_accessor :max
  attr_accessor :applied
  attr_accessor :rules

  MAX_FARE = 3.2
  FARE_ANYWHERE_IN_ZONE_1 = 2.5
  FARE_ONE_ZONE_OUTSIDE_ZONE_1 = 2
  FARE_TWO_ZONES_INCLUDE_ZONE_1 = 3
  FARE_TWO_ZONES_EXCLUDE_ZONE_1 = 2.25
  FARE_THREE_ZONES = 3.2
  FARE_BUS_JOURNEY = 1.8

  def initialize()
    @max = MAX_FARE
    @applied = 0
    @rules = []
  end

  def price(journey)
    if journey.transport.eql?(Transport.bus)
      setup_fare_rules(FARE_BUS_JOURNEY, "FARE_BUS_JOURNEY")
      FARE_BUS_JOURNEY
    elsif journey.has_only_zone_one
      setup_fare_rules(FARE_ANYWHERE_IN_ZONE_1, "FARE_ANYWHERE_IN_ZONE_1")
      FARE_ANYWHERE_IN_ZONE_1
    elsif journey.has_equals_zone && !journey.has_zone_one
      setup_fare_rules(FARE_ONE_ZONE_OUTSIDE_ZONE_1, "FARE_ONE_ZONE_OUTSIDE_ZONE_1")
      FARE_ONE_ZONE_OUTSIDE_ZONE_1
    elsif journey.zones_count.eql?(2) && !journey.has_zone_one
      setup_fare_rules(FARE_TWO_ZONES_EXCLUDE_ZONE_1, "FARE_TWO_ZONES_EXCLUDE_ZONE_1")
      FARE_TWO_ZONES_EXCLUDE_ZONE_1
    elsif journey.zones_count.eql?(2) && journey.has_zone_one
      setup_fare_rules(FARE_TWO_ZONES_INCLUDE_ZONE_1, "FARE_TWO_ZONES_INCLUDE_ZONE_1")
      FARE_TWO_ZONES_INCLUDE_ZONE_1
    elsif journey.zones_count.eql?(3)
      setup_fare_rules(FARE_THREE_ZONES, "FARE_THREE_ZONES")
      FARE_THREE_ZONES
    end
  end

  private

  def setup_fare_rules(fare, rule)
    self.applied += fare
    self.rules.push(rule)
  end
end
