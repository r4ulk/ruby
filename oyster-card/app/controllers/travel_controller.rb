class TravelController < ApplicationController

  def index
    station = Station.new({})
    journeys = [
      Journey.new([station.holborn, station.earls_court_zone_one], Transport.tube),
      Journey.new([station.earls_court_zone_one, station.chelsea], Transport.bus),
      Journey.new([station.earls_court_zone_two, station.hammersmith], Transport.tube),
    ]
    card = Card.new(30)
    travel = Travel.new(journeys, card)
    travel.start();
  end

end
